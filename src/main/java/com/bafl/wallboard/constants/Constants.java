package com.bafl.wallboard.constants;

public class Constants {
	public static final String VDN_ID_LAHORE = "1";
	public static final String CC_ID_LAHORE = "1";

	public static final String VDN_ID_KARACHI = "1";
	public static final String CC_ID_KARACHI = "1";

	/* TEST CONFIG */
	public static final String CMS_GATEWAY_URL_LHR = "http://10.77.194.86:9068/cmsgateway/resource";
	public static final String CMS_GATEWAY_URL_KHI = "http://10.77.194.86:9068/cmsgateway/resource";

	/* LIVE CONFIG */
//		public static final String CMS_GATEWAY_URL_LHR = "http://172.21.31.177:9068/cmsgateway/resource";
//		public static final String CMS_GATEWAY_URL_KHI = "http://172.21.31.177:9068/cmsgateway/resource";

//		public static final String CMS_GATEWAY_URL_KHI = "https://172.24.31.175:9243/cmsgateway/resource";

	public static final String CMS_GATEWAY_REALTIME_SKILL_URL = "/rindex/skill";
	public static final String CMS_GATEWAY_HISTORIC_SKILL_URL = "/hindex/skill";
	public static final String CMS_GATEWAY_HISTORIC_AGENT_INFO_URL = "/hindex/agent";
	public static final String CMS_GATEWAY_REALTIME_VDN_URL = "/rindex/vdn";
	public static final String CMS_GATEWAY_STATEINFO_SKILL_URL = "/real/skill/stateinfo";

	public static String CMS_GATEWAY_VDN_AGENT_INFO_URL = "/synwas/agents/#cc_id#/#vdn_id#";

	/* TEST CONFIG */
	public static String SUPERVISOR_PORTAL_NOTIFICATION_WEBSERVICE_URL = "http://192.168.2.57:8086/sp/api/notification/getActiveNotificationsSkillBased/#skillId#";
	
	/* LIVE CONFIG */
//	 public static String SUPERVISOR_PORTAL_NOTIFICATION_WEBSERVICE_URL =
//	 "http://172.20.3.156:8080/SupervisorPortal/api/notification/getActiveNotificationsSkillBased/#skillId#";

	public static final String SKILL1_ID_LHR = "2";
	public static final String SKILL1_NAME_LHR = "BISP";

	public static final String SKILL2_ID_LHR = "3";
	public static final String SKILL2_NAME_LHR = "BLB";

//	public static final String SKILL1_ID_KHI = "2";
//	public static final String SKILL1_NAME_KHI = "BISP";
//
//	public static final String SKILL2_ID_KHI = "3";
//	public static final String SKILL2_NAME_KHI = "BLB";
	
	public static final String SKILL1_ID_KHI = "1";
	public static final String SKILL1_NAME_KHI = "BISP";

	public static final String SKILL2_ID_KHI = "2";
	public static final String SKILL2_NAME_KHI = "BLB";

	
	public static final String TALKING_AGENTS_INDEX = "IDX_03_02_002";
	public static final String ACW_AGENTS_INDEX = "IDX_03_02_006";
	public static final String ONLINE_AGENTS_INDEX = "IDX_03_02_001";
	public static final String IDLE_AGENTS_INDEX = "IDX_03_02_003";
	public static final String BUSY_AGENTS_INDEX = "IDX_03_02_007";
	public static final String NOTR_AGENTS_INDEX = "IDX_03_02_005";

	public static final String CURRENT_TRAFFIC_IVR_INDEX = "IDX_01_04_002"; // rvdn
	public static final String CURRENT_TRAFFIC_QUEUE_INDEX = "IDX_03_01_002"; // rskill

	public static final String LANDED_CALLS_INDEX = "IDX_03_04_001";
	public static final String ANSWERED_CALLS_INDEX = "IDX_03_04_002";
	public static final String ABANDONED_CALLS_INDEX = "IDX_03_04_003";	
	
	public static final String ABD_CALLS_AFT_THRESHOLD_INDEX = "IDX_03_09_037";
	public static final String ANS_CALLS_AFT_THRESHOLD_INDEX = "IDX_03_09_027";
		
	public static final String WAITING_TIME_INDEX = "IDX_03_06_003";

	public static final String AGENT_TOTAL_CALLS_INDEX = "IDX_04_02_003";
	public static final String AGENT_SERVICE_TIME_INDEX = "IDX_04_02_004";

	public static final String CITY_LAHORE = "1";
	public static final String CITY_KARACHI = "2";

}
