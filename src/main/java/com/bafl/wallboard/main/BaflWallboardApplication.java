package com.bafl.wallboard.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages="com.bafl.wallboard")
public class BaflWallboardApplication {
	public static void main(String[] args) {
		SpringApplication.run(BaflWallboardApplication.class, args);
	}
}
