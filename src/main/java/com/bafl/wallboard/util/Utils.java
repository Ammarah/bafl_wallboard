package com.bafl.wallboard.util;

import com.bafl.wallboard.constants.Constants;

public class Utils {

	public static String getCommaSeperatedSkillsNameForLahore(String skillsIds) {

		if (skillsIds.isEmpty() || skillsIds.equalsIgnoreCase("null") || skillsIds.equalsIgnoreCase(null)) {
			return "N/A";
		}

		String skillIds[] = skillsIds.split(";");
		String skillNames = "";

		for (String skillId : skillIds) {

			if (skillId.equals(Constants.SKILL1_ID_LHR)) {

				if (!skillNames.equals("")) {
					skillNames += "/";
				}

				skillNames = skillNames + Constants.SKILL1_NAME_LHR;
			} else if (skillId.equals(Constants.SKILL2_ID_LHR)) {

				if (!skillNames.equals("")) {
					skillNames += "/";
				}

				skillNames = skillNames + Constants.SKILL2_NAME_LHR;
			}
		}

		if (skillNames.isEmpty()) {
			skillNames = "N/A";
		}

		return skillNames;
	}

	public static String getCommaSeperatedSkillsNameForKarachi(String skillsIds) {

		if (skillsIds.isEmpty() || skillsIds.equalsIgnoreCase("null") || skillsIds.equalsIgnoreCase(null)) {
			return "N/A";
		}

		String skillIds[] = skillsIds.split(";");
		String skillNames = "";

		for (String skillId : skillIds) {

			if (skillId.equals(Constants.SKILL1_ID_KHI)) {

				if (!skillNames.equals("")) {
					skillNames += "/";
				}

				skillNames = skillNames + Constants.SKILL1_NAME_KHI;
			} else if (skillId.equals(Constants.SKILL2_ID_KHI)) {

				if (!skillNames.equals("")) {
					skillNames += "/";
				}

				skillNames = skillNames + Constants.SKILL2_NAME_KHI;
			}
		}

		if (skillNames.isEmpty()) {
			skillNames = "N/A";
		}

		return skillNames;
	}
}
