package com.bafl.wallboard.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.tomcat.util.bcel.Const;
import org.apache.tomcat.util.bcel.classfile.Constant;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bafl.wallboard.constants.Constants;
import com.bafl.wallboard.model.Agent;
import com.bafl.wallboard.model.Notification;
import com.bafl.wallboard.rest.RestClient;
import com.bafl.wallboard.util.AgentAverageHandlingTimeComparator;
import com.bafl.wallboard.util.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Controller
public class WallboardController {

	private static final Logger log = LoggerFactory.getLogger(WallboardController.class);

	JSONParser parser = new JSONParser();
	RestClient restClient = new RestClient();

	static String VDN_ID = null;
	static String CC_ID = null;
	static String CMS_GATEWAY_URL = null;

	@RequestMapping("/")
	public String defaultPage() {
		log.info("Request received on default context = '/'");
		return "index";
	}
	
	@RequestMapping("/test")
	public String hello() {
		log.info("Request received on test context = '/test'");
		return "default";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping("/getStatsForSkill/{skillId}")
	@ResponseBody
	public String getStatsForSkill(@PathVariable("skillId") String skillId) {
		log.info("Request received on context = '/getStatsForSkill' Skill ID: " + skillId);
		JSONObject mergedSkillStats = new JSONObject();
		JSONObject skillStatsKhi = new JSONObject();
		JSONObject skillStatsLhr = new JSONObject();

		if (skillId.equals("1")) {
			log.info("getStatsForSkillBLB | Configuration set to Skill " + skillId);
			// Get Data for BLB Karachi
			//skillStatsKhi = getStatsOfKarachiBLB(Constants.SKILL2_ID_KHI);
			// Get Data for BLB Lahore
			skillStatsLhr = getStatsOfLahoreBLB(Constants.SKILL2_ID_LHR);
		//	mergedSkillStats.putAll(skillStatsKhi);
			mergedSkillStats.putAll(skillStatsLhr);

		} else {
			log.info("getStatsForSkillBISP | Configuration set to Skill " + skillId);
			// Get Data for BISP Karachi
			//skillStatsKhi = getStatsOfKarachiBISP(Constants.SKILL1_ID_KHI);
			// Get Data for BLB Lahore
			skillStatsLhr = getStatsOfLahoreBISP(Constants.SKILL1_ID_LHR);
		//	mergedSkillStats.putAll(skillStatsKhi);
			mergedSkillStats.putAll(skillStatsLhr);

		}

		return mergedSkillStats.toJSONString();
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, Integer> getHistoricStatsForSkill(String skillId, String CMS_GATEWAY_URL, String VDN_ID,
			String CC_ID) {
		log.info("Request received on context = '/getHistoricStatsForSkill' Skill ID: " + skillId);
		HashMap<String, Integer> skillCallStats = new HashMap<String, Integer>();
		JSONObject skillJson;
		JSONArray indexes;

		log.info("getHistoricStatsForSkill | Configuration set to Skill ID=" + skillId + " | VDN ID = " + VDN_ID
				+ " | CC ID = " + CC_ID + " | CMS GATEWAY URL = " + CMS_GATEWAY_URL);

		String jsonResponse = restClient.getSkillBasedStats(new String[] { skillId },
				new String[] { Constants.LANDED_CALLS_INDEX, Constants.ABANDONED_CALLS_INDEX,
						Constants.ANSWERED_CALLS_INDEX, Constants.ANS_CALLS_AFT_THRESHOLD_INDEX,
						Constants.ABD_CALLS_AFT_THRESHOLD_INDEX },
				CMS_GATEWAY_URL + Constants.CMS_GATEWAY_HISTORIC_SKILL_URL, VDN_ID, CC_ID);

		if (jsonResponse == null || jsonResponse.isEmpty()) {
			log.error("Empty or null response received against getHistoricStatsForSkill for Skill ID: " + skillId
					+ " request");
			return null;
		}

		// Parsing response for historic call stats for skill
		try {
			skillJson = (JSONObject) parser.parse(jsonResponse);
			indexes = (JSONArray) ((JSONObject) ((JSONArray) skillJson.get("result")).get(0)).get("idxs");

		} catch (Exception e) {
			return skillCallStats;
		}

		for (int i = 0; i < indexes.size(); i++) {
			JSONObject jsonObject = (JSONObject) indexes.get(i);

			String id = jsonObject.get("id").toString();
			int value = 0;


			//changing get 0r default to get
			if (!String.valueOf(jsonObject.get("val")).equals("null")) {
				value = Integer.valueOf(String.valueOf(jsonObject.get("val")));
			}
			

			if (id.equalsIgnoreCase(Constants.LANDED_CALLS_INDEX)) {
				skillCallStats.put("landed_calls", value);
			} else if (id.equalsIgnoreCase(Constants.ANSWERED_CALLS_INDEX)) {
				skillCallStats.put("answered_calls", value);
			} else if (id.equalsIgnoreCase(Constants.ABANDONED_CALLS_INDEX)) {
				skillCallStats.put("abandoned_calls", value);
			} else if (id.equalsIgnoreCase(Constants.ABD_CALLS_AFT_THRESHOLD_INDEX)) {
				skillCallStats.put("abd_call_threshold", value);
			} else if (id.equalsIgnoreCase(Constants.ANS_CALLS_AFT_THRESHOLD_INDEX)) {
				skillCallStats.put("ans_call_threshold", value);
			}

		}

		return skillCallStats;
	}

	@RequestMapping(value = "/getTopAgents")
	@ResponseBody
	public String getTopAgents() {
		Gson gson = new Gson();
		JsonObject agentsInfoJson = new JsonObject();

		log.info("Request received on context = '/getTopAgents'");
		// Get TopAgnets of Karachi
		
//-----		HashMap<String, Agent> agentInfoMapKhi = getAgentInfoForVdn(Constants.CMS_GATEWAY_URL_KHI,
//				Constants.VDN_ID_KARACHI, Constants.CC_ID_KARACHI);
//		if (agentInfoMapKhi.size() == 0) {
//			return null;
//		}
		
		// Get TopAgnets of Lahore
		HashMap<String, Agent> agentInfoMapLhr = getAgentInfoForVdn(Constants.CMS_GATEWAY_URL_LHR,
				Constants.VDN_ID_LAHORE, Constants.CC_ID_LAHORE);
		if (agentInfoMapLhr.size() == 0) {
			return null;
		}

//----		List<Agent> agentInfoListKhi = getHistoricAgentInfo(Constants.CMS_GATEWAY_URL_KHI, agentInfoMapKhi,
//				Constants.VDN_ID_KARACHI, Constants.CC_ID_KARACHI);
		
		
		List<Agent> agentInfoListLhr = getHistoricAgentInfo(Constants.CMS_GATEWAY_URL_LHR, agentInfoMapLhr,
				Constants.VDN_ID_LAHORE, Constants.CC_ID_LAHORE);

		// Merge both cities top 5 agents list
//		---List<Agent> agentInfoListMerged = new ArrayList<>(agentInfoListKhi);
		List<Agent> agentInfoListMerged = new ArrayList<>();
		agentInfoListMerged.addAll(agentInfoListLhr);

		// sort and filter out the top 5 agents with lowest average handling time
		agentInfoListMerged.sort(new AgentAverageHandlingTimeComparator());
		agentInfoListMerged = agentInfoListMerged.stream().limit(5).collect(Collectors.toList());

		agentsInfoJson.add("data", gson.toJsonTree(agentInfoListMerged));

		return agentsInfoJson.toString();
	}

	private List<Agent> getHistoricAgentInfo(String cmsGatewayUrl, HashMap<String, Agent> agentInfoMap, String vdnId,
			String ccId) {
		List<Agent> agentInfoList = new ArrayList<>();
		JSONObject agentsInfo;

		String[] agentIds = agentInfoMap.keySet().toArray(new String[agentInfoMap.size()]);
		String jsonResponse = restClient.getAgentMontitoringStats(
				cmsGatewayUrl + Constants.CMS_GATEWAY_HISTORIC_AGENT_INFO_URL, agentIds,
				new String[] { Constants.AGENT_TOTAL_CALLS_INDEX, Constants.AGENT_SERVICE_TIME_INDEX }, vdnId, ccId);

		if (jsonResponse == null || jsonResponse.isEmpty()) {
			log.error("Empty or null response received against getTopAgents - getHistoricAgentInfo request");
			return agentInfoList;
		}

		try {
			agentsInfo = (JSONObject) parser.parse(jsonResponse);
		} catch (Exception e) {
			return agentInfoList;
		}

		@SuppressWarnings("unchecked")
		JSONArray resultArray = (JSONArray) agentsInfo.getOrDefault("result", null);

		if (resultArray == null || resultArray.isEmpty()) {
			return agentInfoList;
		}

		for (int i = 0; i < resultArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) resultArray.get(i);
			String agentId = jsonObject.get("id").toString();
			JSONArray idxs = (JSONArray) jsonObject.get("idxs");

			Agent agent = agentInfoMap.get(agentId);

			int totalCalls = 0;
			double serviceTimeInSeconds = 0;
			double averageHandlingTime = 0;
			String serviceTime = null;

			if (!String.valueOf(((JSONObject) idxs.get(0)).get("val")).equals("null")) {
				totalCalls = Integer.valueOf(((JSONObject) idxs.get(0)).get("val").toString());
			}

			if (!String.valueOf(((JSONObject) idxs.get(1)).get("val")).equals("null")) {
				serviceTimeInSeconds = Integer.valueOf(((JSONObject) idxs.get(1)).get("val").toString());
			}

			int serviceTimeSec = (int) serviceTimeInSeconds;

			serviceTime = secondsIntoTimeDuration(serviceTimeSec);

			if (totalCalls != 0) {
				averageHandlingTime = serviceTimeInSeconds / totalCalls;
				int handleTimeSec = (int) averageHandlingTime;

				agent.setTotalCalls(totalCalls);
				agent.setServiceTime(serviceTime);
				agent.setAverageHandlingTime(secondsIntoTimeDuration(handleTimeSec));
				agent.setServiceTimeInSeconds(serviceTimeInSeconds);
				agent.setAverageHandlingTimeSec(averageHandlingTime);

				agentInfoList.add(agent);
			}
		}

		// sort and filter out the top 5 agents with lowest average handling time
		agentInfoList.sort(new AgentAverageHandlingTimeComparator());
		agentInfoList = agentInfoList.stream().limit(5).collect(Collectors.toList());

		// agentsInfoJson.add("data", gson.toJsonTree(agentInfoList));

		return agentInfoList;
	}

	@SuppressWarnings("unchecked")
	private HashMap<String, Agent> getAgentInfoForVdn(String cmsGatewayUrl, String vdnId, String ccId) {
		HashMap<String, Agent> agentInfoMap = new HashMap<>();
		JSONObject agentsInfo = new JSONObject();

		String url = cmsGatewayUrl + Constants.CMS_GATEWAY_VDN_AGENT_INFO_URL.replace("#cc_id#", ccId);
		url = url.replace("#vdn_id#", vdnId);
		String jsonResponse = restClient.getAgentsInfo(url);

		if (jsonResponse == null || jsonResponse.isEmpty()) {
			log.error("Empty or null response received against getTopAgents - getVdnAgentInfo request");
			return agentInfoMap;
		}

		try {
			agentsInfo = (JSONObject) parser.parse(jsonResponse);
		} catch (Exception e) {
			return agentInfoMap;
		}

		JSONArray agentsArray = (JSONArray) agentsInfo.getOrDefault("result", null);
		if (agentsArray == null || agentsArray.isEmpty()) {
			return agentInfoMap;
		}

		for (int i = 0; i < agentsArray.size(); i++) {
			JSONObject obj = (JSONObject) agentsArray.get(i);

			Agent agent = new Agent();
			agent.setAgentId(obj.get("agentNo").toString());
			agent.setAgentName(obj.get("agentName").toString());

			if (cmsGatewayUrl.equalsIgnoreCase(Constants.CMS_GATEWAY_URL_KHI)) {
				agent.setSkill(
						Utils.getCommaSeperatedSkillsNameForKarachi(String.valueOf(obj.getOrDefault("skillId", null))));
			} else {
				agent.setSkill(
						Utils.getCommaSeperatedSkillsNameForLahore(String.valueOf(obj.getOrDefault("skillId", null))));
			}

			agentInfoMap.put(agent.getAgentId(), agent);
		}

		return agentInfoMap;
	}

	public Integer getCurrentIVRTraffic(String cmsGatewayUrl, String CC_ID) {
		JSONObject ivrStatsJson;
		JSONArray indexesArray;
		JSONObject jsonObject;
		Integer ivrTraffic = 0;

		String jsonResponse = restClient.getRealTimeIVRTrafficOnVdn(
				cmsGatewayUrl + Constants.CMS_GATEWAY_REALTIME_VDN_URL,
				new String[] { Constants.CURRENT_TRAFFIC_IVR_INDEX }, CC_ID);
		if (jsonResponse == null || jsonResponse.isEmpty()) {
			log.error("Empty or null response received against getCurrentIVRTraffic request");
			return ivrTraffic;
		}

		// Parsing response for IVR Traffic on Vdn
		try {
			ivrStatsJson = (JSONObject) parser.parse(jsonResponse);
			indexesArray = (JSONArray) ((JSONObject) ((JSONArray) ivrStatsJson.get("result")).get(0)).get("idxs");
			jsonObject = (JSONObject) indexesArray.get(0);
		} catch (Exception e) {
			return ivrTraffic;
		}

		if (jsonObject.get("id").toString().equalsIgnoreCase(Constants.CURRENT_TRAFFIC_IVR_INDEX)) {
			ivrTraffic = Integer.parseInt(jsonObject.get("val").toString());
		}

		return ivrTraffic;
	}

	@RequestMapping(value = "/getNotifications/{skill_id}")
	@ResponseBody
	public String getNotifications(@PathVariable("skill_id") String skillId) {
		log.info("Request received on context = '/getNotifications' Skill ID= " + skillId);

		String url = Constants.SUPERVISOR_PORTAL_NOTIFICATION_WEBSERVICE_URL.replace("#skillId#", skillId);
		List<Notification> notifications = restClient.getNotifications(url);

		if (notifications == null || notifications.size() <= 0) {
			log.error("Empty or null response received against getNotification request");
			return null;
		}

		Gson gson = new Gson();
		JsonObject notificationsJson = new JsonObject();
		notificationsJson.add("data", gson.toJsonTree(notifications));

		return notificationsJson.toString();
	}

	private JSONObject getStatsOfKarachiBLB(String skillId) {
		JSONObject skillStats = new JSONObject();
		JSONObject skillJson = null;
		JSONArray indexes = null;

		int blb_callcenter_khi = 0;

		log.info("getStatsOfKarachiBLB | Configuration set to Skill " + skillId + ", VDN ID = "
				+ Constants.VDN_ID_KARACHI + " | CC ID = " + Constants.CC_ID_KARACHI + " | CMS GATEWAY URL = "
				+ Constants.CMS_GATEWAY_URL_KHI);

		String jsonResponse = restClient.getSkillBasedStats(new String[] { skillId },
				new String[] { Constants.ONLINE_AGENTS_INDEX, Constants.IDLE_AGENTS_INDEX,
						Constants.TALKING_AGENTS_INDEX, Constants.ACW_AGENTS_INDEX, Constants.NOTR_AGENTS_INDEX,
						Constants.CURRENT_TRAFFIC_QUEUE_INDEX },
				Constants.CMS_GATEWAY_URL_KHI + Constants.CMS_GATEWAY_REALTIME_SKILL_URL, Constants.VDN_ID_KARACHI,
				Constants.CC_ID_KARACHI);

		if (jsonResponse == null || jsonResponse.isEmpty()) {
			log.error("Empty or null response received against getRealTimeStatsForSkill for Skill ID: " + skillId
					+ " request");
			// {"bisp_callcenter_khi":1,"bisp_abandoned_khi":1,"bisp_khi_busy":1,"bisp_khi_idle":0,"bisp_khi_notr":0,"bisp_threshold_khi":1,"bisp_agents_khi":1,"bisp_waiting_khi":"00:00:00","bisp_service_lvl_khi":33,"bisp_landed_khi":3,"bisp_queue_khi":0,"bisp_answered_khi":2,"bisp_ivr_khi":0}
//			skillStats.put("blb_agents_khi", 0);
//			skillStats.put("blb_khi_idle", 0);
//			skillStats.put("blb_khi_busy", 0);
//			skillStats.put("blb_khi_notr", 0);
//			skillStats.put("blb_queue_khi", 0);
//			skillStats.put("blb_callcenter_khi", 0);
//			skillStats.put("blb_ivr_khi", 0);
//			skillStats.put("blb_landed_khi", 0);
//			skillStats.put("blb_answered_khi", 0);
//			skillStats.put("blb_abandoned_khi", 0);
//			skillStats.put("blb_threshold_khi", 0);
//			skillStats.put("blb_waiting_khi", "00:00:00");
//			skillStats.put("blb_service_lvl_khi", 0);
			return skillStats;
		}

		// Parsing response for realtime skill stats for skill
		try {
			skillJson = (JSONObject) parser.parse(jsonResponse);
			indexes = (JSONArray) ((JSONObject) ((JSONArray) skillJson.get("result")).get(0)).get("idxs");
		} catch (Exception e) {
			return skillStats;
		}

		for (int i = 0; i < indexes.size(); i++) {
			JSONObject jsonObject = (JSONObject) indexes.get(i);

			String id = jsonObject.get("id").toString();
			int value = Integer.valueOf(jsonObject.get("val").toString());

			if (id.equalsIgnoreCase(Constants.ONLINE_AGENTS_INDEX)) {
				skillStats.put("blb_agents_khi", value);
			} else if (id.equalsIgnoreCase(Constants.IDLE_AGENTS_INDEX)) {
				skillStats.put("blb_khi_idle", value);
			} else if (id.equalsIgnoreCase(Constants.TALKING_AGENTS_INDEX)) {
				blb_callcenter_khi += value;

			} else if (id.equalsIgnoreCase(Constants.ACW_AGENTS_INDEX)) {
				blb_callcenter_khi += value;
				// busy agents=talking+working == call center traffic
				skillStats.put("blb_khi_busy", blb_callcenter_khi);

			} else if (id.equalsIgnoreCase(Constants.NOTR_AGENTS_INDEX)) {
				skillStats.put("blb_khi_notr", value);
			} else if (id.equalsIgnoreCase(Constants.CURRENT_TRAFFIC_QUEUE_INDEX)) {
				skillStats.put("blb_queue_khi", value);
			}
		}

		skillStats.put("blb_callcenter_khi", blb_callcenter_khi);
		skillStats.put("blb_ivr_khi", getCurrentIVRTraffic(Constants.CMS_GATEWAY_URL_KHI, Constants.CC_ID_KARACHI));

		HashMap<String, Integer> historicStats = getHistoricStatsForSkill(skillId, Constants.CMS_GATEWAY_URL_KHI,
				Constants.VDN_ID_KARACHI, Constants.CC_ID_KARACHI);
		//chnaging  get 0r default to get
		skillStats.put("blb_landed_khi", historicStats.get("landed_calls"));
		skillStats.put("blb_answered_khi", historicStats.get("answered_calls"));
		skillStats.put("blb_abandoned_khi", historicStats.get("abandoned_calls"));
		skillStats.put("blb_threshold_khi", historicStats.get("abd_call_threshold"));
		skillStats.put("blb_waiting_khi", getAvgWaitingTime(skillId, Constants.CMS_GATEWAY_URL_LHR,
				Constants.VDN_ID_LAHORE, Constants.CC_ID_LAHORE));
		
		Integer ans_call_threshold = historicStats.getOrDefault("ans_call_threshold",0);

		Integer totalCalls = historicStats.getOrDefault("answered_calls",0)
				+ historicStats.getOrDefault("abandoned_calls",0);
		Integer totalCallsAfterThrh = ans_call_threshold + historicStats.getOrDefault("abd_call_threshold",0);

		Integer blb_service_lvl_khi = 0;
		if (totalCalls > 0) {
			blb_service_lvl_khi = (((totalCalls) - (totalCallsAfterThrh)) * 100) / totalCalls;
		}
		skillStats.put("blb_service_lvl_khi", blb_service_lvl_khi);

		return skillStats;

	}

	private JSONObject getStatsOfLahoreBLB(String skillId) {
		JSONObject skillStats = new JSONObject();
		JSONObject skillJson = null;
		JSONArray indexes = null;

		int blb_callcenter_lhr = 0;

		log.info("getStatsOfLahoreBLB | Configuration set to Skill " + skillId + ", VDN ID = " + Constants.VDN_ID_LAHORE
				+ " | CC ID = " + Constants.CC_ID_LAHORE + " | CMS GATEWAY URL = " + Constants.CMS_GATEWAY_URL_LHR);

		String jsonResponse = restClient.getSkillBasedStats(new String[] { skillId },
				new String[] { Constants.ONLINE_AGENTS_INDEX, Constants.IDLE_AGENTS_INDEX,
						Constants.TALKING_AGENTS_INDEX, Constants.ACW_AGENTS_INDEX, Constants.NOTR_AGENTS_INDEX,
						Constants.CURRENT_TRAFFIC_QUEUE_INDEX },
				Constants.CMS_GATEWAY_URL_LHR + Constants.CMS_GATEWAY_REALTIME_SKILL_URL, Constants.VDN_ID_LAHORE,
				Constants.CC_ID_LAHORE);

		if (jsonResponse == null || jsonResponse.isEmpty()) {
			log.error("Empty or null response received against getRealTimeStatsForSkill for Skill ID: " + skillId
					+ " request");
			return skillStats;
		}

		// Parsing response for realtime skill stats for skill
		try {
			skillJson = (JSONObject) parser.parse(jsonResponse);
			indexes = (JSONArray) ((JSONObject) ((JSONArray) skillJson.get("result")).get(0)).get("idxs");
		} catch (Exception e) {
			return skillStats;
		}

		for (int i = 0; i < indexes.size(); i++) {
			JSONObject jsonObject = (JSONObject) indexes.get(i);

			String id = jsonObject.get("id").toString();
			int value = Integer.valueOf(jsonObject.get("val").toString());

			if (id.equalsIgnoreCase(Constants.ONLINE_AGENTS_INDEX)) {
				skillStats.put("blb_agents_lhr", value);
			} else if (id.equalsIgnoreCase(Constants.IDLE_AGENTS_INDEX)) {
				skillStats.put("blb_lhr_idle", value);
			} else if (id.equalsIgnoreCase(Constants.TALKING_AGENTS_INDEX)) {
				blb_callcenter_lhr += value;

			} else if (id.equalsIgnoreCase(Constants.ACW_AGENTS_INDEX)) {
				blb_callcenter_lhr += value;
				// busy agents=talking+working == call center traffic
				skillStats.put("blb_lhr_busy", blb_callcenter_lhr);

			} else if (id.equalsIgnoreCase(Constants.NOTR_AGENTS_INDEX)) {
				skillStats.put("blb_lhr_notr", value);
			} else if (id.equalsIgnoreCase(Constants.CURRENT_TRAFFIC_QUEUE_INDEX)) {
				skillStats.put("blb_queue_lhr", value);
			}
		}

		skillStats.put("blb_callcenter_lhr", blb_callcenter_lhr);
		skillStats.put("blb_ivr_lhr", getCurrentIVRTraffic(Constants.CMS_GATEWAY_URL_LHR, Constants.CC_ID_LAHORE));

		HashMap<String, Integer> historicStats = getHistoricStatsForSkill(skillId, Constants.CMS_GATEWAY_URL_LHR,
				Constants.VDN_ID_LAHORE, Constants.CC_ID_LAHORE);
		skillStats.put("blb_landed_lhr", historicStats.get("landed_calls"));
		skillStats.put("blb_answered_lhr", historicStats.get("answered_calls"));
		skillStats.put("blb_abandoned_lhr", historicStats.get("abandoned_calls"));
		skillStats.put("blb_threshold_lhr", historicStats.get("abd_call_threshold"));
		skillStats.put("blb_waiting_lhr", getAvgWaitingTime(skillId, Constants.CMS_GATEWAY_URL_LHR,
				Constants.VDN_ID_LAHORE, Constants.CC_ID_LAHORE));

		Integer ans_call_threshold = historicStats.getOrDefault("ans_call_threshold",0);

		Integer totalCalls = historicStats.getOrDefault("answered_calls",0)
				+ historicStats.getOrDefault("abandoned_calls",0);
		Integer totalCallsAfterThrh = ans_call_threshold + historicStats.getOrDefault("abd_call_threshold",0);
		Integer blb_service_lvl_lhr = 0;
		if (totalCalls > 0) {
			blb_service_lvl_lhr = (((totalCalls) - (totalCallsAfterThrh)) * 100) / totalCalls;
		}
		skillStats.put("blb_service_lvl_lhr", blb_service_lvl_lhr);

		return skillStats;

	}

	private JSONObject getStatsOfKarachiBISP(String skillId) {
		JSONObject skillStats = new JSONObject();
		JSONObject skillJson = null;
		JSONArray indexes = null;

		int bisp_callcenter_khi = 0;

		log.info("getStatsOfKarachiBISP | Configuration set to Skill " + skillId + ", VDN ID = "
				+ Constants.VDN_ID_KARACHI + " | CC ID = " + Constants.CC_ID_KARACHI + " | CMS GATEWAY URL = "
				+ Constants.CMS_GATEWAY_URL_KHI);

		String jsonResponse = restClient.getSkillBasedStats(new String[] { skillId },
				new String[] { Constants.ONLINE_AGENTS_INDEX, Constants.IDLE_AGENTS_INDEX,
						Constants.TALKING_AGENTS_INDEX, Constants.ACW_AGENTS_INDEX, Constants.NOTR_AGENTS_INDEX,
						Constants.CURRENT_TRAFFIC_QUEUE_INDEX },
				Constants.CMS_GATEWAY_URL_KHI + Constants.CMS_GATEWAY_REALTIME_SKILL_URL, Constants.VDN_ID_KARACHI,
				Constants.CC_ID_KARACHI);

		if (jsonResponse == null || jsonResponse.isEmpty()) {
			log.error("Empty or null response received against getRealTimeStatsForSkill for Skill ID: " + skillId
					+ " request");
			// {"bisp_callcenter_khi":1,"bisp_abandoned_khi":1,"bisp_khi_busy":1,"bisp_khi_idle":0,"bisp_khi_notr":0,"bisp_threshold_khi":1,"bisp_agents_khi":1,"bisp_waiting_khi":"00:00:00","bisp_service_lvl_khi":33,"bisp_landed_khi":3,"bisp_queue_khi":0,"bisp_answered_khi":2,"bisp_ivr_khi":0}
//						skillStats.put("bisp_agents_khi", 0);
//						skillStats.put("bisp_khi_idle", 0);
//						skillStats.put("bisp_khi_busy", 0);
//						skillStats.put("bisp_khi_notr", 0);
//						skillStats.put("bisp_queue_khi", 0);
//						skillStats.put("bisp_callcenter_khi", 0);
//						skillStats.put("bisp_ivr_khi", 0);
//						skillStats.put("bisp_landed_khi", 0);
//						skillStats.put("bisp_answered_khi", 0);
//						skillStats.put("bisp_abandoned_khi", 0);
//						skillStats.put("bisp_threshold_khi", 0);
//						skillStats.put("bisp_waiting_khi", "00:00:00");
//						skillStats.put("bisp_service_lvl_khi", 0);
			return skillStats;
		}

		// Parsing response for realtime skill stats for skill
		try {
			skillJson = (JSONObject) parser.parse(jsonResponse);
			indexes = (JSONArray) ((JSONObject) ((JSONArray) skillJson.get("result")).get(0)).get("idxs");
		} catch (Exception e) {
			return skillStats;
		}

		for (int i = 0; i < indexes.size(); i++) {
			JSONObject jsonObject = (JSONObject) indexes.get(i);

			String id = jsonObject.get("id").toString();
			int value = Integer.valueOf(jsonObject.get("val").toString());

			if (id.equalsIgnoreCase(Constants.ONLINE_AGENTS_INDEX)) {
				skillStats.put("bisp_agents_khi", value);
			} else if (id.equalsIgnoreCase(Constants.IDLE_AGENTS_INDEX)) {
				skillStats.put("bisp_khi_idle", value);

			} else if (id.equalsIgnoreCase(Constants.TALKING_AGENTS_INDEX)) {
				bisp_callcenter_khi += value;

			} else if (id.equalsIgnoreCase(Constants.ACW_AGENTS_INDEX)) {
				bisp_callcenter_khi += value;
				// busy agents=talking+working == call center traffic
				skillStats.put("bisp_khi_busy", bisp_callcenter_khi);

			} else if (id.equalsIgnoreCase(Constants.NOTR_AGENTS_INDEX)) {
				skillStats.put("bisp_khi_notr", value);
			} else if (id.equalsIgnoreCase(Constants.CURRENT_TRAFFIC_QUEUE_INDEX)) {
				skillStats.put("bisp_queue_khi", value);
			}
		}

		skillStats.put("bisp_callcenter_khi", bisp_callcenter_khi);
		skillStats.put("bisp_ivr_khi", getCurrentIVRTraffic(Constants.CMS_GATEWAY_URL_KHI, Constants.CC_ID_KARACHI));

		HashMap<String, Integer> historicStats = getHistoricStatsForSkill(skillId, Constants.CMS_GATEWAY_URL_KHI,
				Constants.VDN_ID_KARACHI, Constants.CC_ID_KARACHI);
		skillStats.put("bisp_landed_khi", historicStats.get("landed_calls"));
		skillStats.put("bisp_answered_khi", historicStats.get("answered_calls"));
		skillStats.put("bisp_abandoned_khi", historicStats.get("abandoned_calls"));
		skillStats.put("bisp_threshold_khi", historicStats.get("abd_call_threshold"));
		skillStats.put("bisp_waiting_khi", getAvgWaitingTime(skillId, Constants.CMS_GATEWAY_URL_LHR,
				Constants.VDN_ID_LAHORE, Constants.CC_ID_LAHORE));

		Integer ans_call_threshold = historicStats.getOrDefault("ans_call_threshold",0);

		Integer totalCalls = historicStats.getOrDefault("answered_calls",0)
				+ historicStats.getOrDefault("abandoned_calls",0);
		Integer totalCallsAfterThrh = ans_call_threshold + historicStats.getOrDefault("abd_call_threshold",0);

		Integer bisp_service_lvl_khi = 0;
		if (totalCalls > 0) {
			bisp_service_lvl_khi = (((totalCalls) - (totalCallsAfterThrh)) * 100) / totalCalls;
		}
		skillStats.put("bisp_service_lvl_khi", bisp_service_lvl_khi);

		return skillStats;

	}

	private JSONObject getStatsOfLahoreBISP(String skillId) {
		JSONObject skillStats = new JSONObject();
		JSONObject skillJson = null;
		JSONArray indexes = null;

		int bisp_callcenter_lhr = 0;

		log.info("getStatsOfLahoreBISP | Configuration set to Skill " + skillId + ", VDN ID = "
				+ Constants.VDN_ID_LAHORE + " | CC ID = " + Constants.CC_ID_LAHORE + " | CMS GATEWAY URL = "
				+ Constants.CMS_GATEWAY_URL_LHR);

		String jsonResponse = restClient.getSkillBasedStats(new String[] { skillId },
				new String[] { Constants.ONLINE_AGENTS_INDEX, Constants.IDLE_AGENTS_INDEX,
						Constants.TALKING_AGENTS_INDEX, Constants.ACW_AGENTS_INDEX, Constants.NOTR_AGENTS_INDEX,
						Constants.CURRENT_TRAFFIC_QUEUE_INDEX },
				Constants.CMS_GATEWAY_URL_LHR + Constants.CMS_GATEWAY_REALTIME_SKILL_URL, Constants.VDN_ID_LAHORE,
				Constants.CC_ID_LAHORE);

		if (jsonResponse == null || jsonResponse.isEmpty()) {
			log.error("Empty or null response received against getRealTimeStatsForSkill for Skill ID: " + skillId
					+ " request");
			return skillStats;
		}

		// Parsing response for realtime skill stats for skill
		try {
			skillJson = (JSONObject) parser.parse(jsonResponse);
			indexes = (JSONArray) ((JSONObject) ((JSONArray) skillJson.get("result")).get(0)).get("idxs");
		} catch (Exception e) {
			return skillStats;
		}

		for (int i = 0; i < indexes.size(); i++) {
			JSONObject jsonObject = (JSONObject) indexes.get(i);

			String id = jsonObject.get("id").toString();
			int value = Integer.valueOf(jsonObject.get("val").toString());

			if (id.equalsIgnoreCase(Constants.ONLINE_AGENTS_INDEX)) {
				skillStats.put("bisp_agents_lhr", value);
			} else if (id.equalsIgnoreCase(Constants.IDLE_AGENTS_INDEX)) {
				skillStats.put("bisp_lhr_idle", value);
			} else if (id.equalsIgnoreCase(Constants.TALKING_AGENTS_INDEX)) {
				bisp_callcenter_lhr += value;

			} else if (id.equalsIgnoreCase(Constants.ACW_AGENTS_INDEX)) {
				bisp_callcenter_lhr += value;
				// busy agents=talking+working == call center traffic
				skillStats.put("bisp_lhr_busy", bisp_callcenter_lhr);

			} else if (id.equalsIgnoreCase(Constants.NOTR_AGENTS_INDEX)) {
				skillStats.put("bisp_lhr_notr", value);
			} else if (id.equalsIgnoreCase(Constants.CURRENT_TRAFFIC_QUEUE_INDEX)) {
				skillStats.put("bisp_queue_lhr", value);
			}
		}

		skillStats.put("bisp_callcenter_lhr", bisp_callcenter_lhr);
		skillStats.put("bisp_ivr_lhr", getCurrentIVRTraffic(Constants.CMS_GATEWAY_URL_LHR, Constants.CC_ID_LAHORE));

		HashMap<String, Integer> historicStats = getHistoricStatsForSkill(skillId, Constants.CMS_GATEWAY_URL_LHR,
				Constants.VDN_ID_LAHORE, Constants.CC_ID_LAHORE);
		skillStats.put("bisp_landed_lhr", historicStats.get("landed_calls"));
		skillStats.put("bisp_answered_lhr", historicStats.get("answered_calls"));
		skillStats.put("bisp_abandoned_lhr", historicStats.get("abandoned_calls"));
		skillStats.put("bisp_threshold_lhr", historicStats.get("abd_call_threshold"));
		skillStats.put("bisp_waiting_lhr", getAvgWaitingTime(skillId, Constants.CMS_GATEWAY_URL_LHR,
				Constants.VDN_ID_LAHORE, Constants.CC_ID_LAHORE));

		Integer ans_call_threshold = historicStats.getOrDefault("ans_call_threshold",0);

		Integer totalCalls = historicStats.getOrDefault("answered_calls",0)
				+ historicStats.getOrDefault("abandoned_calls",0);
		Integer totalCallsAfterThrh = ans_call_threshold + historicStats.getOrDefault("abd_call_threshold",0);
		Integer bisp_service_lvl_lhr = 0;
		if (totalCalls > 0) {
			bisp_service_lvl_lhr = (((totalCalls) - (totalCallsAfterThrh)) * 100) / totalCalls;

		}

		skillStats.put("bisp_service_lvl_lhr", bisp_service_lvl_lhr);

		return skillStats;

	}

	@SuppressWarnings("unchecked")
	public String getAvgWaitingTime(String skillId, String CMS_GATEWAY_URL, String VDN_ID, String CC_ID) {
		log.info("Request received on context = '/getAvgWaitingTime' Skill ID: " + skillId);
		String avgWaitingTime = "00:00:00";
		JSONObject skillJson;
		log.info("getAvgWaitingTime | Configuration set to Skill ID=" + skillId + " | VDN ID = " + VDN_ID
				+ " | CC ID = " + CC_ID + " | CMS GATEWAY URL = " + CMS_GATEWAY_URL);

		String jsonResponse = restClient.getSkillDetailedStats(new String[] { skillId },
				CMS_GATEWAY_URL + Constants.CMS_GATEWAY_STATEINFO_SKILL_URL, VDN_ID, CC_ID);

		if (jsonResponse == null || jsonResponse.isEmpty()) {
			log.error(
					"Empty or null response received against getAvgWaitingTime for Skill ID: " + skillId + " request");
			return null;
		}
		// Parsing response for average waiting time for skill
		try {
			skillJson = (JSONObject) parser.parse(jsonResponse);
			JSONArray result = (JSONArray) skillJson.get("result");
			for (int i = 0; i < result.size(); i++) {
				JSONObject jsonObject = (JSONObject) result.get(i);

				long avgWaitTimeSecond = (long) jsonObject.get("evenWaitTimeIn5");
				Integer timeInseconds = (int) (long) avgWaitTimeSecond;
				avgWaitingTime = secondsIntoTimeDuration(timeInseconds);
			}

		} catch (Exception e) {
			return avgWaitingTime;
		}

		return avgWaitingTime;
	}

	public String secondsIntoTimeDuration(int sec) {
		String time = "00:00:00";
		// int day = (int) TimeUnit.SECONDS.toDays(avgWaitTimeSecond);
		long hours = TimeUnit.SECONDS.toHours(sec);
		long minute = TimeUnit.SECONDS.toMinutes(sec) - (TimeUnit.SECONDS.toHours(sec) * 60);
		long second = TimeUnit.SECONDS.toSeconds(sec) - (TimeUnit.SECONDS.toMinutes(sec) * 60);
		if (hours >= 1) {
			return String.format("%02d:%02d:%02d", hours, minute, second);
		} else if (minute >= 1) {
			return String.format("00:%02d:%02d", minute, second);
		} else {
			time = String.format("00:00:%02d", second);
		}

		return time;

	}

}
