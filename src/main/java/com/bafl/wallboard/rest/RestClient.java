package com.bafl.wallboard.rest;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import com.bafl.wallboard.constants.Constants;
import com.bafl.wallboard.model.Notification;

public class RestClient {

	// private static final Logger log = LoggerFactory.getLogger(RestClient.class);

	@SuppressWarnings({ "unchecked", "deprecation" })
	public String getSkillBasedStats(String[] skillId, String[] indexIds, String url, String vdnId, String ccId) {
		// Setting headers.
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		// Setting object which will be sent as post object.
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("ccId", ccId);
		jsonObj.put("vdnId", vdnId);
		jsonObj.put("objectIds", skillId);
		jsonObj.put("indexIds", indexIds);

		// Setting HttpEntity
		HttpEntity<JSONObject> requestObject = new HttpEntity<>(jsonObj, headers);

		RestTemplate request = new RestTemplate();
		ResponseEntity<String> response = request.exchange(url, HttpMethod.POST, requestObject, String.class);

		return response.getBody();
	}

	@SuppressWarnings({ "deprecation" })
	public String getAgentsInfo(String url) {
		// Setting headers.
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		// Setting HttpEntity
		HttpEntity<JSONObject> requestObject = new HttpEntity<>(null, headers);

		RestTemplate request = new RestTemplate();
		ResponseEntity<String> response = request.exchange(url, HttpMethod.GET, requestObject, String.class);

		return response.getBody();
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public String getAgentMontitoringStats(String url, String[] agentIds, String[] indexIds, String vdnId,
			String ccId) {
		// Setting headers.
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		// Setting object which will be sent as post object.
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("ccId", ccId);
		jsonObj.put("vdnId", vdnId);
		jsonObj.put("objectIds", agentIds);
		jsonObj.put("indexIds", indexIds);

		// Setting HttpEntity
		HttpEntity<JSONObject> requestObject = new HttpEntity<>(jsonObj, headers);

		RestTemplate request = new RestTemplate();
		ResponseEntity<String> response = request.exchange(url, HttpMethod.POST, requestObject, String.class);

		return response.getBody();
	}

	@SuppressWarnings({ "deprecation" })
	public List<Notification> getNotifications(String url) {
		// Setting headers.
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		// Setting HttpEntity
		HttpEntity<JSONObject> requestObject = new HttpEntity<>(null, headers);

		RestTemplate request = new RestTemplate();
		ResponseEntity<List<Notification>> response = request.exchange(url, HttpMethod.GET, requestObject,
				new ParameterizedTypeReference<List<Notification>>() {
				});

		return response.getBody();
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public String getRealTimeIVRTrafficOnVdn(String url, String[] indexIds, String ccId) {
		// Setting headers.
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		// Setting object which will be sent as post object.
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("ccId", ccId); 
		jsonObj.put("objectIds", new String[] { Constants.VDN_ID_LAHORE, Constants.VDN_ID_KARACHI });
		jsonObj.put("indexIds", indexIds);

		// Setting HttpEntity
		HttpEntity<JSONObject> requestObject = new HttpEntity<>(jsonObj, headers);

		RestTemplate request = new RestTemplate();
		ResponseEntity<String> response = request.exchange(url, HttpMethod.POST, requestObject, String.class);

		return response.getBody();
	}

	@SuppressWarnings("deprecation")
	public String getWeather(String url, String cityName) {
		// Setting headers.
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		// Setting HttpEntity
		HttpEntity<JSONObject> requestObject = new HttpEntity<>(null, headers);

		RestTemplate request = new RestTemplate();
		ResponseEntity<String> response = request.exchange(url, HttpMethod.GET, requestObject, String.class);

		return response.getBody();
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public String getSkillDetailedStats(String[] skillId, String url, String vdnId, String ccId) {
		// Setting headers.
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		// Setting object which will be sent as post object.
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("ccId", ccId);
		jsonObj.put("vdnId", vdnId);
		jsonObj.put("skillIds", skillId);

		// Setting HttpEntity
		HttpEntity<JSONObject> requestObject = new HttpEntity<>(jsonObj, headers);

		RestTemplate request = new RestTemplate();
		ResponseEntity<String> response = request.exchange(url, HttpMethod.POST, requestObject, String.class);

		return response.getBody();
	}
	public static void main(String[] args) {
//		List<Notification> notifications = getNotifications(
//				"http://192.168.2.57:8086/sp/api/notification/getActiveNotifications/1");
//		System.out.println("Notifications: " + notifications.get(0).getStartDateTime());

//		String response = getAgentMontitoringStats(Constants.CMS_GATEWAY_HISTORIC_AGENT_INFO_URL,
//				new String[] { "101", "102", "103" }, new String[] { Constants.AGENT_TOTAL_CALLS_INDEX });
//		System.out.println("Response: " + response);
//		

//		String response1 = getAgentsInfo(Constants.CMS_GATEWAY_VDN_AGENT_INFO_URL);
//		System.out.println("Response: " + response1);

//		String response = getSkillBasedStats(new String[] {"9"},  new String[] { Constants.ONLINE_AGENTS_INDEX } ,Constants.CMS_GATEWAY_REALTIME_SKILL_URL);
//		System.out.println("Response: " + response);
	}

}
