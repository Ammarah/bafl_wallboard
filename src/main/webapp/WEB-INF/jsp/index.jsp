<!DOCTYPE HTML>
<html>

<head>

<title>Wall Board</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<!--css-->
<link rel="stylesheet" href="css/wallboard.css">
<link rel="stylesheet" href="css/Chart.css">
<!---->

<!--scripts-->
<script type="text/javascript" src="js/loader.js"></script>
<script src="js/Chart.bundle.min.js"></script>
<script src="js/constants.js"></script>
<script src="js/weather.js"></script>
<script src="js/bisp_stats.js"></script>
<script src="js/blb_stats.js"></script>
<!--scripts-->

</head>

<body>

	<div class="container-fluid" style="padding: 25px;">
		<div id="BLB" class="tab1">
			<div class="row">
				<div class="col-lg-12 text-center box-skill">
					<span>BLB</span>
					<button class="b1" id="btn1" onclick="SwitchSkill()">Switched
						to BISP</button>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2 text-center box-size">
					<h1 id="total_blb_agents">0</h1>
					<span>Total Login Agents </span>
				</div>
				<div class="col-lg-2 text-center box-khi">
					<h1 id="blb_agents_khi">0</h1>
					<span># of <br>agents<br> <strong>KARACHI</strong>
					</span>
				</div>
				<div class="col-lg-2 p-0">
					<div class="col-lg-12 ul-row-khi">
						<span id="khi_idle" class="box-count">0</span><span>#of idle
							agents </span>
					</div>
					<div class="col-lg-12 ul-row-khi">
						<span id="khi_busy" class="box-count">0</span><span>#of busy
							agents </span>
					</div>
					<div class="col-lg-12 ul-row-khi">
						<span id="khi_notr" class="box-count">0</span><span>#of not
							ready agents</span>
					</div>
				</div>
				<div class="col-lg-1 text-center weather-khi">
					<img id="tkb_img" style="height: 40px; width: 40px;" class="img"
						src="https://vortex.accuweather.com/adc2010/images/slate/icons/1.svg"
						alt="Weather">
					<p id="tkb_sky"></p>
					<h3 class="temperature" id="tkb_temp"></h3>
					<div class="hi" id="tkb_hi"></div>
					<div class="low" id="tkb_low"></div>

				</div>
				<div class="col-lg-2 text-center box-lhr">
					<h1 id="blb_agents_lhr">0</h1>
					<span># of <br>agents<br> <strong>LAHORE</strong></span>
				</div>
				<div class="col-lg-2 p-0">
					<div class="col-lg-12 ul-row-lhr">
						<span id="lhr_idle" class="box-count">0</span><span>#of idle
							agents </span>
					</div>
					<div class="col-lg-12 ul-row-lhr">
						<span id="lhr_busy" class="box-count">0</span><span>#of busy
							agents </span>
					</div>
					<div class="col-lg-12 ul-row-lhr">
						<span id="lhr_notr" class="box-count">0</span><span>#of not
							ready agents</span>
					</div>
				</div>
				<div class="col-lg-1 text-center weather-lhr">
					<img id="tlb_img" style="height: 40px; width: 40px;" class="img"
						src="https://vortex.accuweather.com/adc2010/images/slate/icons/1.svg"
						alt="Weather">
					<p id="tlb_sky"></p>
					<h3 class="temperature" id="tlb_temp"></h3>
					<div class="hi" id="tlb_hi"></div>
					<div class="low" id="tlb_low"></div>
				</div>
			</div>
			<div class="row calls">
				<div class="text-center box-queue">
					<h1 id="blb_queue">0</h1>
					<br> <span>Calls in Queue </span>
				</div>
				<div class="text-center box-wait">
					<h1 id="blb_waiting">00:00:00</h1>
					<br> <span>Waiting Duration </span>
				</div>
				<div class="text-center box-landed">
					<h1 id="blb_landed">0</h1>
					<br> <span>Landed Calls </span>
				</div>
				<div class="text-center box-answerd">
					<h1 id="blb_answered">0</h1>
					<br> <span>Answered Calls </span>
				</div>
				<div class="text-center box-threshold">
					<h1 id="blb_threshold">0</h1>
					<span>Abd. Calls Aft. Threshold</span>
				</div>
				<div class="text-center box-service">
					<h1 id="blb_service_lvl">0</h1>
					<br> <span>Service Level </span>
				</div>
				<div class="text-center box-abandoned">
					<h1 id="blb_abandoned">0</h1>
					<br> <span>Abandoned Calls </span>
				</div>

			</div>

			<div class="row">
				<div class="col-lg-12 text-center box-slides"></div>
			</div>
			<div class="slideshow">
				<div class="row">
					<div class="col-lg-6 text-center w-box">
						<p>Top 5 BLB Agents</p>
						<div class="row" style="margin: 0px;">
							<table class="col-lg-12 table table-responsive">
								<thead>
									<tr>
										<th>#</th>
										<th>AgentID</th>
										<th>Total Calls</th>
										<th>Service time</th>
										<th>Avg Handle Time</th>
									</tr>
								</thead>
								<tbody id="agentInfoRowsBLB">

								</tbody>
							</table>
						</div>
					</div>
					<div class="col-lg-6 text-center w-box">
						<p>Agent Stats BLB</p>
						<canvas id="barchart_blb_agent" width="200" height="52"></canvas>
					</div>
				</div>
			</div>
			<div class="slideshow">
				<div class="row">
					<div class="col-lg-4 text-center w-box">
						<p>Call Stats BLB</p>
						<canvas id="piechart_blb" width="200" height="75"></canvas>
					</div>
					<div class="col-lg-4 text-center w-box">
						<p>Current Traffic BLB</p>
						<canvas id="barchart_blb" width="200" height="80"></canvas>
					</div>
					<div class="col-lg-4 text-center w-box">
						<p>Notifications</p>
						<div class="row" style="margin: 0px; height: 160px;"
							id="notificationsBLB"></div>
					</div>
				</div>
			</div>
		</div>


		<div id="BISP" class="tab2">
			<div class="row">
				<div class="col-lg-12 text-center  box-skill">
					<span>BISP</span>
					<button class="b2" id="btn2" onclick="SwitchSkill()">Switched
						to BLB</button>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2 text-center box-size">
					<h1 id="total_bisp_agents">0</h1>
					<span>Total Login Agents </span>
				</div>
				<div class="col-lg-2 text-center box-khi">
					<h1 id="bisp_agents_khi">0</h1>
					<span># of <br>agents<br> <strong>KARACHI</strong>
					</span>
				</div>
				<div class="col-lg-2 p-0">
					<div class="col-lg-12 ul-row-khi">
						<span id="bisp_khi_idle" class="box-count">0</span><span>#of
							idle agents </span>
					</div>
					<div class="col-lg-12 ul-row-khi">
						<span id="bisp_khi_busy" class="box-count">0</span><span>#of
							busy agents </span>
					</div>
					<div class="col-lg-12 ul-row-khi">
						<span id="bisp_khi_notr" class="box-count">0</span><span>#of
							not ready agents</span>
					</div>
				</div>
				<div class="col-lg-1 text-center weather-khi">
					<img id="tkp_img" style="height: 40px; width: 40px;" class="img"
						src="https://vortex.accuweather.com/adc2010/images/slate/icons/1.svg"
						alt="Weather">
					<p id="tkp_sky"></p>
					<h3 class="temperature" id="tkp_temp"></h3>
					<div class="hi" id="tkp_hi"></div>
					<div class="low" id="tkp_low"></div>
				</div>
				<div class="col-lg-2 text-center box-lhr">
					<h1 id="bisp_agents_lhr">0</h1>
					<span># of <br>agents<br> <strong>LAHORE</strong></span>
				</div>
				<div class="col-lg-2 p-0">
					<div class="col-lg-12 ul-row-lhr">
						<span id="bisp_lhr_idle" class="box-count">0</span><span>#of
							idle agents </span>
					</div>
					<div class="col-lg-12 ul-row-lhr">
						<span id="bisp_lhr_busy" class="box-count">0</span><span>#of
							busy agents </span>
					</div>
					<div class="col-lg-12 ul-row-lhr">
						<span id="bisp_lhr_notr" class="box-count">0</span><span>#of
							not ready agents</span>
					</div>
				</div>
				<div class="col-lg-1 text-center weather-lhr">
					<img id="tlp_img" style="height: 40px; width: 40px;" class="img"
						src="https://vortex.accuweather.com/adc2010/images/slate/icons/1.svg"
						alt="Weather">
					<p id="tlp_sky"></p>
					<h3 class="temperature" id="tlp_temp"></h3>
					<div class="hi" id="tlp_hi"></div>
					<div class="low" id="tlp_low"></div>
				</div>
			</div>
			<div class="row calls">
				<div class="text-center box-queue">
					<h1 id="bisp_queue">0</h1>
					<br> <span>Calls in Queue </span>
				</div>
				<div class="text-center box-wait">
					<h1 id="bisp_waiting">00:00:00</h1>
					<br> <span>Waiting Duration </span>
				</div>
				<div class="text-center box-landed">
					<h1 id="bisp_landed">0</h1>
					<br> <span>Landed Calls </span>
				</div>
				<div class="text-center box-answerd">
					<h1 id="bisp_answered">0</h1>
					<br> <span>Answered Calls </span>
				</div>
				<div class="text-center box-threshold">
					<h1 id="bisp_threshold">0</h1>
					<span>Abd. Calls Aft. Threshold</span>
				</div>
				<div class="text-center box-service">
					<h1 id="bisp_service_lvl">0</h1>
					<br> <span>Service Level </span>
				</div>
				<div class="text-center box-abandoned">
					<h1 id="bisp_abandoned">0</h1>
					<br> <span>Abandoned Calls </span>
				</div>

			</div>

			<div class="row">
				<div class="col-lg-12 text-center box-slides"></div>
			</div>
			<div class="slideshow-1">
				<div class="row">
					<div class="col-lg-6 text-center w-box">
						<p>Top 5 BISP Agents</p>
						<div class="row" style="margin: 0px;">
							<table class="col-lg-12 table table-responsive">
								<thead>
									<tr>
										<th>#</th>
										<th>AgentID</th>
										<th>Total Calls</th>
										<th>Service time</th>
										<th>Avg Handle Time</th>
									</tr>
								</thead>
								<tbody id="agentInfoRowsBISP">

								</tbody>
							</table>
						</div>
					</div>
					<div class="col-lg-6 text-center w-box">
						<p>Agent Stats BISP</p>
						<canvas id="barchart_bisp_agent" width="200" height="52"></canvas>
					</div>
				</div>
			</div>
			<div class="slideshow-1">
				<div class="row">
					<div class="col-lg-4 text-center w-box">
						<p>Call Stats BISP</p>
						<canvas id="piechart_bisp" width="200" height="75"></canvas>
					</div>
					<div class="col-lg-4 text-center w-box">
						<p>Current Traffic BISP</p>
						<canvas id="barchart_bisp" width="200" height="80"></canvas>
					</div>
					<div class="col-lg-4 text-center w-box">
						<p>Notification</p>
						<div class="row" style="margin: 0px; height: 160px;"
							id="notificationsBISP"></div>
					</div>
				</div>
			</div>
		</div>

	</div>

</body>
<script src="js/w3.js"></script>
<script src="js/jquery.min.js"></script>
<script>
	!function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (!d.getElementById(id)) {
			js = d.createElement(s);
			js.id = id;
			js.src = 'https://weatherwidget.io/js/widget.min.js';
			fjs.parentNode.insertBefore(js, fjs);
		}
	}(document, 'script', 'weatherwidget-io-js');

	!function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (!d.getElementById(id)) {
			js = d.createElement(s);
			js.id = id;
			js.src = 'https://weatherwidget.io/js/widget.min.js';
			fjs.parentNode.insertBefore(js, fjs);
		}
	}(document, 'script', 'weatherwidget-io-js');
</script>
<script>
	$('button').click(function() {
		if (this.id == 'btn1') {

			$('.tab2').show();
			$('.tab1').hide();
		} else {

			$('.tab2').hide();
			$('.tab1').show();
		}
	})
</script>
<script>
	myShow = w3.slideshow(".slideshow", 6000);
</script>
<script>
	myShow1 = w3.slideshow(".slideshow-1", 6000);
</script>

<script>
	$(document).ready(init);

// 	setInterval(init, 5000);
var getTop5AgentsTimer = null;
var getNotificationsTimer = null;
var getStatsForSkillBISPTimer = null;
var getStatsForSkillBLBTimer = null;

function init() {
	
	if (localStorage.skillName !== "undefined") {
		if (localStorage.skillName === "BLB") {
			skill_id="1";
			$('.tab2').hide();
			$('.tab1').show();
	    } else {
			skill_id="2";
			$('.tab2').show();
			$('.tab1').hide();
	    }
	}
	else{
		localStorage.skillName === "BLB";
	}
	
	//	resetStats();
// 		getNotifications();
// 		getTop5Agents();
		 getTop5AgentsTimer = setTimeout(getTop5Agents, 2000);
		  getNotificationsTimer = setTimeout(getNotifications, 2000);

		if (skill_id == 2) {

			//getStatsForSkillBISP();
			  getStatsForSkillBISPTimer = setTimeout(getStatsForSkillBISP, 2000);

		} else if(skill_id == 1) {
			//getStatsForSkillBLB();
			  getStatsForSkillBLBTimer = setTimeout(getStatsForSkillBLB, 2000);

		}

	};



	function getStatsForSkillBLB() {
		console.log("Getting StatsForSkillBLB");

		$
				.ajax({
					url : "/wallboard/getStatsForSkill/" + skill_id,
					dataType : "json",
					success : function(result) {
                        clearTimeout(getStatsForSkillBLBTimer);
                        
						if (isEmpty(result)) {
							console.log("Empty response from server");
							getStatsForSkillBLBTimer = setTimeout(getStatsForSkillBLB, 5000);

							return;
						}
						//Karachi Stats
// 						blb_agents_khi = result.blb_agents_khi;
// 						blb_khi_idle = result.blb_khi_idle;
// 						blb_khi_busy = result.blb_khi_busy;
// 						blb_khi_notr = result.blb_khi_notr;

// 						blb_queue_khi = result.blb_queue_khi;
// 						blb_callcenter_khi = result.blb_callcenter_khi;
// 						blb_ivr_khi = result.blb_ivr_khi;

// 						blb_landed_khi = result.blb_landed_khi;
// 						blb_answered_khi = result.blb_answered_khi;
// 						blb_abandoned_khi = result.blb_abandoned_khi;
						
// 						blb_waiting_khi=result.blb_waiting_khi;
// 						blb_threshold_khi=result.blb_threshold_khi;
// 						blb_service_lvl_khi=result.blb_service_lvl_khi;

						//Lahore Stats

						blb_agents_lhr = result.blb_agents_lhr;
						blb_lhr_idle = result.blb_lhr_idle;
						blb_lhr_busy = result.blb_lhr_busy;
						blb_lhr_notr = result.blb_lhr_notr;

						blb_queue_lhr = result.blb_queue_lhr;
						blb_callcenter_lhr = result.blb_callcenter_lhr;
						blb_ivr_lhr = result.blb_ivr_lhr;
						
						if(result.blb_landed_lhr !== null){
						blb_landed_lhr = result.blb_landed_lhr;}
						if(result.blb_answered_lhr !== null){
						blb_answered_lhr = result.blb_answered_lhr;}
						if(result.blb_abandoned_lhr !== null){
						blb_abandoned_lhr = result.blb_abandoned_lhr;}
						
						
						blb_waiting_lhr=result.blb_waiting_lhr;
						if(result.blb_threshold_lhr !== null){
						blb_threshold_lhr=result.blb_threshold_lhr;}
						if(result.blb_service_lvl_lhr!==0){
						blb_service_lvl_lhr=result.blb_service_lvl_lhr;}
						//Comulative BLB Call center Stats

						total_blb_agents = blb_agents_khi + blb_agents_lhr;
						blb_answered = blb_answered_khi + blb_answered_lhr;
						blb_landed = blb_landed_khi + blb_landed_lhr;
						blb_abandoned = blb_abandoned_khi + blb_abandoned_lhr;
					
						blb_waiting = getAverageTime([blb_waiting_khi,blb_waiting_lhr]);
						blb_ivr = blb_ivr_khi + blb_ivr_lhr;
						blb_queue = blb_queue_khi + blb_queue_lhr;
						blb_callcenter = blb_callcenter_khi
								+ blb_callcenter_lhr;
						blb_threshold = blb_threshold_khi + blb_threshold_lhr;
						blb_service_lvl = (blb_service_lvl_khi + blb_service_lvl_lhr) / 2;

						//Agents Stats
						$('#blb_agents_khi').html(blb_agents_khi);
						$('#blb_agents_lhr').html(blb_agents_lhr);
						$('#total_blb_agents').html(total_blb_agents);
						$('#lhr_idle').html(blb_lhr_idle);
						$('#lhr_busy').html(blb_lhr_busy);
						$('#lhr_notr').html(blb_lhr_notr);
						$('#khi_idle').html(blb_khi_idle);
						$('#khi_busy').html(blb_khi_busy);
						$('#khi_notr').html(blb_khi_notr);
						//Calls Stats
						$('#blb_queue').html(blb_queue);
						$('#blb_waiting').html(blb_waiting);
						$('#blb_landed').html(blb_landed);
						$('#blb_answered').html(blb_answered);
						$('#blb_threshold').html(blb_threshold);
						$('#blb_service_lvl').html(blb_service_lvl);
						$('#blb_abandoned').html(blb_abandoned);

						//console.log("blb khi :" + blb_agents_khi);
						updateBlbStats();
						
						getStatsForSkillBLBTimer = setTimeout(getStatsForSkillBLB, 5000);
					},
					error: function (xhr, status, error) {
                        clearTimeout(getStatsForSkillBLBTimer);
						getStatsForSkillBLBTimer = setTimeout(getStatsForSkillBLB, 5000);
                    }
				});
	};

	function getStatsForSkillBISP() {
		console.log("Getting StatsForSkillBISP");

		$
				.ajax({
					url : "/wallboard/getStatsForSkill/" + skill_id,
					dataType : "json",
					success : function(result) {
                        clearTimeout(getStatsForSkillBISPTimer);

						if (isEmpty(result)) {
							console.log("Empty response from server");
							getStatsForSkillBISPTimer = setTimeout(getStatsForSkillBISP, 5000);

							return;
						}
						//Karachi Stats

// 						bisp_agents_khi = result.bisp_agents_khi;
// 						bisp_khi_idle = result.bisp_khi_idle;
// 						bisp_khi_busy = result.bisp_khi_busy;
// 						bisp_khi_notr = result.bisp_khi_notr;

// 						bisp_queue_khi = result.bisp_queue_khi;
// 						bisp_callcenter_khi = result.bisp_callcenter_khi;
// 						bisp_ivr_khi = result.bisp_ivr_khi;

// 						bisp_landed_khi = result.bisp_landed_khi;
// 						bisp_answered_khi = result.bisp_answered_khi;
// 						bisp_abandoned_khi = result.bisp_abandoned_khi;
						
// 						bisp_waiting_khi = result.bisp_waiting_khi;
// 						bisp_threshold_khi=result.bisp_threshold_khi;
// 						bisp_service_lvl_khi=result.bisp_service_lvl_khi;
					
						//Lahore Stats
						
						bisp_agents_lhr = result.bisp_agents_lhr;
						bisp_lhr_idle = result.bisp_lhr_idle;
						bisp_lhr_busy = result.bisp_lhr_busy;
						bisp_lhr_notr = result.bisp_lhr_notr;

						bisp_queue_lhr = result.bisp_queue_lhr;
						bisp_callcenter_lhr = result.bisp_callcenter_lhr;
						bisp_ivr_lhr = result.bisp_ivr_lhr;

						if(result.bisp_landed_lhr !== null){
						bisp_landed_lhr = result.bisp_landed_lhr;}
						if(result.bisp_answered_lhr !== null){
						bisp_answered_lhr = result.bisp_answered_lhr;}
						if(result.bisp_abandoned_lhr !== null){
						bisp_abandoned_lhr = result.bisp_abandoned_lhr;}
						
						bisp_waiting_lhr=result.bisp_waiting_lhr;
						if(result.bisp_threshold_lhr !== null){
						bisp_threshold_lhr=result.bisp_threshold_lhr;}
						if(result.bisp_service_lvl_lhr!==0){
						bisp_service_lvl_lhr=result.bisp_service_lvl_lhr;}

						//Comulative BLB Call center Stats
						total_bisp_agents = bisp_agents_khi + bisp_agents_lhr;
						bisp_answered = bisp_answered_khi + bisp_answered_lhr;
						bisp_landed = bisp_landed_khi + bisp_landed_lhr;
						bisp_abandoned = bisp_abandoned_khi
								+ bisp_abandoned_lhr;
						bisp_waiting = getAverageTime([bisp_waiting_khi,bisp_waiting_lhr]);
						bisp_ivr = bisp_ivr_khi + bisp_ivr_lhr;
						bisp_queue = bisp_queue_khi + bisp_queue_lhr;
						bisp_callcenter = bisp_callcenter_khi
								+ bisp_callcenter_lhr;
						bisp_threshold = bisp_threshold_khi + bisp_threshold_lhr;
						bisp_service_lvl = (bisp_service_lvl_khi + bisp_service_lvl_lhr) / 2;

						//Agents Stats to front end
						$('#bisp_agents_khi').html(bisp_agents_khi);
						$('#bisp_agents_lhr').html(bisp_agents_lhr);
						$('#total_bisp_agents').html(total_bisp_agents);
						$('#bisp_lhr_idle').html(bisp_lhr_idle);
						$('#bisp_lhr_busy').html(bisp_lhr_busy);
						$('#bisp_lhr_notr').html(bisp_lhr_notr);
						$('#bisp_khi_idle').html(bisp_khi_idle);
						$('#bisp_khi_busy').html(bisp_khi_busy);
						$('#bisp_khi_notr').html(bisp_khi_notr);
						//Calls Stats to front end
						$('#bisp_queue').html(bisp_queue);
						$('#bisp_waiting').html(bisp_waiting);
						$('#bisp_landed').html(bisp_landed);
						$('#bisp_answered').html(bisp_answered);
						$('#bisp_threshold').html(bisp_threshold);
						$('#bisp_service_lvl').html(bisp_service_lvl);
						$('#bisp_abandoned').html(bisp_abandoned);

						updateBispStats();
						getStatsForSkillBISPTimer = setTimeout(getStatsForSkillBISP, 5000);
					},
					error: function (xhr, status, error) {
                        clearTimeout(getStatsForSkillBISPTimer);
						getStatsForSkillBISPTimer = setTimeout(getStatsForSkillBISP, 5000);
                    }
				});

	};

	function getTop5Agents() {
		console.log("Getting top 5 agents");
		$
				.ajax({
					url : "/wallboard/getTopAgents",
					dataType : "json",
					success : function(result) {
                        clearTimeout(getTop5AgentsTimer);

						console.log("Get Top Agents Result " + result);
						if ((isEmpty(result))|| result.data.length==0) {
							console.log("result is empty");
	                        getTop5AgentsTimer = setTimeout(getTop5Agents, 5000);

							return;
						}
						var sNo=0;
						if(skill_id == 1) {
							$("#agentInfoRowsBLB").empty();

						for (var i = 0; i < result.data.length; i++) {
							sNo++;
							var agent = result.data[i];
							$("#agentInfoRowsBLB").append(
									'<tr><td>'+ sNo +'</td><td>' + agent.agentId + '</td><td>'
											+ agent.totalCalls + '</td><td>'
											+ agent.serviceTime + '</td><td>'
											+ agent.averageHandlingTime
											+ '</td></tr>');
						}
					}
						else if(skill_id == 2) {
							$("#agentInfoRowsBISP").empty();

							for (var i = 0; i < result.data.length; i++) {
								sNo++;
								var agent = result.data[i];
								$("#agentInfoRowsBISP").append(
										'<tr><td>'+ sNo +'</td><td>' + agent.agentId + '</td><td>'
												+ agent.totalCalls + '</td><td>'
												+ agent.serviceTime + '</td><td>'
												+ agent.averageHandlingTime
												+ '</td></tr>');
							}
						}
						getTop5AgentsTimer = setTimeout(getTop5Agents, 5000);
					},
					error: function (xhr, status, error) {
                        clearTimeout(getTop5AgentsTimer);
                        getTop5AgentsTimer = setTimeout(getTop5Agents, 5000);
                    }
				});
	};
	
	function getNotifications() {
		console.log("Getting Notifications");
		var skill_name="";
		if(skill_id=="1"){
			skill_name="BLB";
		}
		else if (skill_id=="2"){
			skill_name="BISP";
			}
		$
				.ajax({
					url : "/wallboard/getNotifications/" + skill_name,
					dataType : "json",
					success : function(result) {
                        clearTimeout(getNotificationsTimer);

						if (isEmpty(result)) {
							console.log("result is emmpty");
	                        getNotificationsTimer = setTimeout(getNotifications, 5000);

							return;
						}
						if(skill_id == 1) {
						$("#notificationsBLB").empty();

						for (var i = 0; i < result.data.length; i++) {
							var notification = result.data[i];
							$("#notificationsBLB")
									.append(
											'<div class="col-lg-12 text-center" id="notification-div-blb"><div class="card w3-green"><h3 class="agent-state-text" style="margin-top: 6px; font-size: 15px;">'
													+ notification.notificationText
													+ '</h3></div></div>');

						}
					}
						else if(skill_id == 2) {
							$("#notificationsBISP").empty();

							for (var i = 0; i < result.data.length; i++) {
								var notification = result.data[i];
								$("#notificationsBISP")
										.append(
												'<div class="col-lg-12 text-center" id="notification-div-bisp"><div class="card w3-green"><h3 class="agent-state-text" style="margin-top: 6px; font-size: 15px;">'
														+ notification.notificationText
														+ '</h3></div></div>');

							}
						}
						getNotificationsTimer = setTimeout(getNotifications, 5000);
					},
					error: function (xhr, status, error) {
                        clearTimeout(getNotificationsTimer);
                        getNotificationsTimer = setTimeout(getNotifications, 5000);
                    }
				});
	};
	
	
	function resetStats() {
		//bisp stats
		//Agents Stats to front end
		$('#bisp_agents_khi').text(0);
		$('#bisp_agents_lhr').text(0);
		$('#total_bisp_agents').text(0);
		$('#bisp_lhr_idle').text(0);
		$('#bisp_lhr_busy').text(0);
		$('#bisp_lhr_notr').text(0);
		$('#bisp_khi_idle').text(0);
		$('#bisp_khi_busy').text(0);
		$('#bisp_khi_notr').text(0);
		//Calls Stats to front end
		$('#bisp_queue').text(0);
		$('#bisp_waiting').text(0);
		$('#bisp_landed').text(0);
		$('#bisp_answered').text(0);
		$('#bisp_threshold').text(0);
		$('#bisp_service_lvl').text(0);
		$('#bisp_abandoned').text(0);
		// BISP varaiables
		 bisp_answered_khi = 0;
		 bisp_landed_khi = 0;
		 bisp_abandoned_khi = 0;

		 bisp_waiting_khi = '00:00:00';

		 bisp_ivr_khi = 0;
		 bisp_queue_khi = 0;
		 bisp_callcenter_khi = 0;

		 bisp_threshold_khi = 0;
		 bisp_service_lvl_khi = 0;
		 
		 bisp_answered_lhr = 0;
		 bisp_landed_lhr = 0;
		 bisp_abandoned_lhr = 0;

		 bisp_waiting_lhr = '00:00:00';

		 bisp_ivr_lhr = 0;
		 bisp_queue_lhr = 0;
		 bisp_callcenter_lhr = 0;

		 bisp_threshold_lhr = 0;
		 bisp_service_lvl_lhr = 0;

		 bisp_ivr = 0;
		 bisp_queue = 0;
		 bisp_callcenter = 0;
		
		//blb stats
		//Agents Stats
		$('#blb_agents_khi').text(0);
		$('#blb_agents_lhr').text(0);
		$('#total_blb_agents').text(0);
		$('#lhr_idle').text(0);
		$('#lhr_busy').text(0);
		$('#lhr_notr').text(0);
		$('#khi_idle').text(0);
		$('#khi_busy').text(0);
		$('#khi_notr').text(0);
			//Calls Stats
		$('#blb_queue').text(0);
		$('#blb_waiting').text(0);
		$('#blb_landed').text(0);
	    $('#blb_answered').text(0);
		$('#blb_threshold').text(0);
		$('#blb_service_lvl').text(0);
		$('#blb_abandoned').text(0);
			//BLB variables
		 blb_answered_khi = 0;
		 blb_landed_khi = 0;
		 blb_abandoned_khi = 0;

		 blb_waiting_khi = '00:00:00';

		 blb_ivr_khi = 0;
		 blb_queue_khi = 0;
		 blb_callcenter_khi = 0;

		 blb_threshold_khi = 0;
		 blb_service_lvl_khi = 0;

		 blb_answered_lhr = 0;
		 blb_landed_lhr = 0;
		 blb_abandoned_lhr = 0;

		 blb_waiting_lhr = '00:00:00';

		 blb_ivr_lhr = 0;
		 blb_queue_lhr = 0;
		 blb_callcenter_lhr = 0;

		 blb_threshold_lhr = 0;
		 blb_service_lvl_lhr = 0;

		 blb_ivr = 0;
		 blb_queue = 0;
		 blb_callcenter = 0;	
	}

	

	function SwitchSkill() {
		
		if (skill_id == 1) {
			skill_id = 2; ////1 for BLB 2 for BISP 
			localStorage.skillName = "BISP";
			console.log("Skill is Switched to BISP-----"+ new Date().toLocaleString());

		} else {
			skill_id = 1;
			localStorage.skillName = "BLB";
			console.log("Skill is Switched to BLB-------"+new Date().toLocaleString());
		}

        clearTimeout(getStatsForSkillBISPTimer);
        clearTimeout(getStatsForSkillBLBTimer);
        clearTimeout(getNotificationsTimer);
        clearTimeout(getTop5AgentsTimer);

		init();
	};
	
	
	function getAverageTime(array) {
		var times="00:00:00";
		if((array[0]==="00:00:00" && array[1]==="00:00:00")||(array[0]==null && array[1]==null)){
			return times;
		}
		else{
			if(array[0]==null || (array[0]==="00:00:00")){
				return array[1];
			}
			else if(array[1]==null || (array[1]==="00:00:00")){
				return array[0];
			}
			else {		
	    	 times = [3600, 60, 1],
	        parts = array.map(s => s.split(':').reduce((s, v, i) => s + times[i] * v, 0)),
	        avg = Math.round(parts.reduce((a, b) => a + b, 0) / parts.length);

	    	return times.map(t => [Math.floor(avg / t), avg %= t][0]).map(v => v.toString().padStart(2, 0)).join(':');
				}
		    
			
			}
	
	}

	function isEmpty(obj) {
	    for(var key in obj) {
	        if(obj.hasOwnProperty(key))
	            return false;
	    }
	    return true;
	}
	
	
    var time = new Date().getTime();
    $(document.body).bind("mousemove keypress", function(e) {
        time = new Date().getTime();
    });

    function refresh() {
        if(new Date().getTime() - time >= 1200000) {
        	//if there is inactivity from last 20 mins, then refresh
        	console.log("localStorage.skillName =" + localStorage.skillName+ "--------"+new Date().getTime());
        	 clearTimeout(getStatsForSkillBISPTimer);
             clearTimeout(getStatsForSkillBLBTimer);
             clearTimeout(getNotificationsTimer);
             clearTimeout(getTop5AgentsTimer);
        	window.location.reload(true);
        }
        else 
            setTimeout(refresh, 10000);
    }

    setTimeout(refresh, 10000);

</script>


</html>