window.addEventListener('load', function(){


    $.ajax({
        url: 'http://api.openweathermap.org/data/2.5/group?id=1172451,1174872&units=metric&appid=b8bddf5feff7f546ee313dd9f0e46400', // returns "[1,2,3,4,5,6]"
        dataType: 'json', // jQuery will parse the response as JSON
        success: function (result) {
            
            $('#lhr_w_blb').html('Lhr '+Math.floor(result.list[0].main.temp)+'&#8451;<br> Rain '+result.list[0].main.humidity+'%');
            $('#khi_w_blb').html('Khi '+Math.floor(result.list[1].main.temp)+'&#8451;<br> Rain '+result.list[1].main.humidity+'%');
            $('#lhr_w_bisp').html('Lhr '+Math.floor(result.list[0].main.temp)+'&#8451;<br> Rain '+result.list[0].main.humidity+'%');
            $('#khi_w_bisp').html('Khi '+Math.floor(result.list[1].main.temp)+'&#8451;<br> Rain '+result.list[1].main.humidity+'%');
            
            // Weather for BLB
            if(result.list[1].clouds.all < 5)
            {   
                $('#tkb_sky').html('<br>Sunny');
                $('#tkb_img').attr("src", "images/sunny.svg");
                $('#tkp_sky').html('<br>Sunny');
                $('#tkp_img').attr("src", "images/sunny.svg");
            }
            else if(result.list[1].clouds.all > 5 && result.list[1].clouds.all < 26)
            {
                $('#tkb_sky').html('Mostly<br>Sunny');
                $('#tkb_img').attr("src", "images/mostly-sunny.svg");
                $('#tkp_sky').html('Mostly<br>Sunny');
                $('#tkp_img').attr("src", "images/mostly-sunny.svg");
            }
            else if(result.list[1].clouds.all > 25 && result.list[1].clouds.all < 46)
            {
                $('#tkb_sky').html('Partially<br>Cloudy');
                $('#tkb_img').attr("src", "images/part-cloudy.svg");
                $('#tkp_sky').html('Partially<br>Cloudy');
                $('#tkp_img').attr("src", "images/part-cloudy.svg");
            }
            else if(result.list[1].clouds.all > 45 && result.list[1].clouds.all < 70)
            {
                $('#tkb_sky').html('Mostly<br>Cloudy');
                $('#tkb_img').attr("src", "images/mostly-cloudy.svg");
                $('#tkp_sky').html('Mostly<br>Cloudy');
                $('#tkp_img').attr("src", "images/mostly-cloudy.svg");
            }
            else{
                $('#tkb_sky').html('<br>Cloudy');
                $('#tkb_img').attr("src", "images/cloudy.svg");
                $('#tkp_sky').html('<br>Cloudy');
                $('#tkp_img').attr("src", "images/cloudy.svg");
            }
            if(result.list[0].clouds.all < 5)
            {   
                $('#tlb_sky').html('<br>Sunny');
                $('#tlb_img').attr("src", "images/sunny.svg");
                $('#tlp_sky').html('<br>Sunny');
                $('#tlp_img').attr("src", "images/sunny.svg");
            }
            else if(result.list[0].clouds.all > 5 && result.list[0].clouds.all < 26)
            {
                $('#tlb_sky').html('Mostly<br>Sunny');
                $('#tlb_img').attr("src", "images/mostly-sunny.svg");
                $('#tlp_sky').html('Mostly<br>Sunny');
                $('#tlp_img').attr("src", "images/mostly-sunny.svg");
            }
            else if(result.list[0].clouds.all > 25 && result.list[0].clouds.all < 46)
            {
                $('#tlb_sky').html('Partially<br>Cloudy');
                $('#tlb_img').attr("src", "images/part-cloudy.svg");
                $('#tlp_sky').html('Partially<br>Cloudy');
                $('#tlp_img').attr("src", "images/part-cloudy.svg");
            }
            else if(result.list[0].clouds.all > 45 && result.list[0].clouds.all < 70)
            {
                $('#tlb_sky').html('Mostly<br>Cloudy');
                $('#tlb_img').attr("src", "images/mostly-cloudy.svg");
                $('#tlp_sky').html('Mostly<br>Cloudy');
                $('#tlp_img').attr("src", "images/mostly-cloudy.svg");
            }
            else{
                $('#tlb_sky').html('<br>Cloudy');
                $('#tlb_img').attr("src", "images/cloudy.svg");
                $('#tlp_sky').html('<br>Cloudy');
                $('#tlp_img').attr("src", "images/cloudy.svg");
            }          
            //Weather for BLB Karachi
            $('#tkb_temp').html(Math.floor(result.list[1].main.temp)+'&#8451;');
            $('#tkb_hi').html(Math.floor(result.list[1].main.temp_max)+'&#8451;');
            $('#tkb_low').html(Math.floor(result.list[1].main.temp_min)+'&#8451;');
            //Weather for BLB Lahore
            $('#tlb_temp').html(Math.floor(result.list[0].main.temp)+'&#8451;');
            $('#tlb_hi').html(Math.floor(result.list[0].main.temp_max)+'&#8451;');
            $('#tlb_low').html(Math.floor(result.list[0].main.temp_min)+'&#8451;');

            // Weather for BISP Karachi 
            $('#tkp_temp').html(Math.floor(result.list[1].main.temp)+'&#8451;');
            $('#tkp_hi').html(Math.floor(result.list[1].main.temp_max)+'&#8451;');
            $('#tkp_low').html(Math.floor(result.list[1].main.temp_min)+'&#8451;');
            //Weather for BISP Lahore
            $('#tlp_temp').html(Math.floor(result.list[0].main.temp)+'&#8451;');
            $('#tlp_hi').html(Math.floor(result.list[0].main.temp_max)+'&#8451;');
            $('#tlp_low').html(Math.floor(result.list[0].main.temp_min)+'&#8451;');
        }
    });
});