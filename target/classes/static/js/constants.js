//BLB variables

//Karachi Call center Stats
var blb_answered_khi = 0;
var blb_landed_khi = 0;
var blb_abandoned_khi = 0;

var blb_waiting_khi = '00:00:00';

var blb_ivr_khi = 0;
var blb_queue_khi = 0;
var blb_callcenter_khi = 0;

var blb_threshold_khi = 0;
var blb_service_lvl_khi = 0;

var blb_agents_khi = 0;

var blb_khi_idle = 0;
var blb_khi_busy = 0;
var blb_khi_notr = 0;


//Lahore Call Center Stats

var blb_answered_lhr = 0;
var blb_landed_lhr = 0;
var blb_abandoned_lhr = 0;

var blb_waiting_lhr = '00:00:00';

var blb_ivr_lhr = 0;
var blb_queue_lhr = 0;
var blb_callcenter_lhr = 0;

var blb_threshold_lhr = 0;
var blb_service_lvl_lhr = 0;

var blb_agents_lhr = 0;

var blb_lhr_idle = 0;
var blb_lhr_busy = 0;
var blb_lhr_notr = 0;


var total_blb_agents = 0;
var blb_answered = 0;
var blb_landed = 0;
var blb_abandoned = 0;
var blb_waiting = '00:00:00';
var blb_ivr = 0;
var blb_queue = 0;
var blb_callcenter = 0;
var blb_threshold = 0;
var blb_service_lvl = 0;

// BISP varaiables

// Karachi Call center Stats
var bisp_answered_khi = 0;
var bisp_landed_khi = 0;
var bisp_abandoned_khi = 0;

var bisp_waiting_khi = '00:00:00';

var bisp_ivr_khi = 0;
var bisp_queue_khi = 0;
var bisp_callcenter_khi = 0;

var bisp_threshold_khi = 0;
var bisp_service_lvl_khi = 0;

var bisp_agents_khi = 0;

var bisp_khi_idle = 0;
var bisp_khi_busy = 0;
var bisp_khi_notr = 0;

//Lahore Call Center Stats

var bisp_answered_lhr = 0;
var bisp_landed_lhr = 0;
var bisp_abandoned_lhr = 0;

var bisp_waiting_lhr = '00:00:00';

var bisp_ivr_lhr = 0;
var bisp_queue_lhr = 0;
var bisp_callcenter_lhr = 0;

var bisp_threshold_lhr = 0;
var bisp_service_lvl_lhr = 0;

var bisp_agents_lhr = 0;

var bisp_lhr_idle = 0;
var bisp_lhr_busy = 0;
var bisp_lhr_notr = 0;


var total_bisp_agents = 0;
var bisp_answered = 0;
var bisp_landed = 0;
var bisp_abandoned = 0;
var bisp_waiting = '00:00:00';
var bisp_ivr = 0;
var bisp_queue = 0;
var bisp_callcenter = 0;
var bisp_threshold = 0;
var bisp_service_lvl = 0;

var skill_id = 1; // 1 for BLB 2 for BISP
